package com.sbuy.order.unit.mock;

import com.sbuy.order.client.GoodsClient;
import com.sbuy.order.dto.base.DataDTO;
import com.sbuy.order.dto.base.PhoneDTO;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Component
public class GoodsClientMock implements GoodsClient {
    @Override
    public DataDTO<List<PhoneDTO>> getPhones(List<Long> ids) {
        DataDTO<List<PhoneDTO>> data = new DataDTO<>();
        PhoneDTO one = new PhoneDTO();
        one.setId(1L);
        one.setName("Nokia 8800");
        one.setDescription("The best phone what I have ever seen");
        one.setImage("/phones/1/image");
        one.setPrice(new BigDecimal(150.00));
        PhoneDTO two = new PhoneDTO();
        two.setId(2L);
        two.setName("Iphone x");
        two.setDescription("Just Iphone");
        two.setImage("/phones/2/image");
        two.setPrice(new BigDecimal(1000));
        data.setData(Arrays.asList(one, two));
        return data;
    }
}
