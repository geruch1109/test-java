package com.sbuy.order.unit;

import com.sbuy.order.dto.request.ItemRequest;
import com.sbuy.order.dto.request.OrderRequest;
import com.sbuy.order.exception.EntityNotFoundException;
import com.sbuy.order.model.ItemEntity;
import com.sbuy.order.model.OrderEntity;
import com.sbuy.order.service.OrderService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Ignore
public class OrderServiceTest {

    @Autowired
    private OrderService orderService;

    @Test
    public void orderSuccess() {
        assertEquals(prepareValidOrder(), orderService.order(prepareValidOrderData()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void orderUnSuccessPhoneDoesNotExist() {
        orderService.order(prepareNotValidOrderData());
    }

    private OrderRequest prepareValidOrderData() {
        OrderRequest order = new OrderRequest();
        order.setName("Name");
        order.setSurname("Surname");
        order.setEmail("email@email.com");
        List<ItemRequest> items = new ArrayList<>();
        ItemRequest one = new ItemRequest();
        one.setItemId(1L);
        one.setCount(2);
        ItemRequest two = new ItemRequest();
        two.setItemId(2L);
        two.setCount(3);
        items.add(one);
        items.add(two);
        order.setOrder(items);
        return order;
    }

    private OrderRequest prepareNotValidOrderData() {
        OrderRequest order = new OrderRequest();
        order.setName("Name");
        order.setSurname("Surname");
        order.setEmail("email@email.com");
        List<ItemRequest> items = new ArrayList<>();
        ItemRequest one = new ItemRequest();
        one.setItemId(9L);
        one.setCount(2);
        ItemRequest two = new ItemRequest();
        two.setItemId(10L);
        two.setCount(3);
        items.add(one);
        items.add(two);
        order.setOrder(items);
        return order;
    }

    private OrderEntity prepareValidOrder() {
        OrderEntity order = new OrderEntity();
        order.setName("Name");
        order.setSurname("Surname");
        order.setEmail("email@email.com");
        List<ItemEntity> items = new ArrayList<>();
        ItemEntity one = new ItemEntity();
        one.setId(1L);
        one.setName("Nokia 8800");
        one.setDescription("The best phone what I have ever seen");
        one.setImage("/phones/1/image");
        one.setPrice(new BigDecimal(150.00));
        one.setCount(2);
        ItemEntity two = new ItemEntity();
        two.setId(2L);
        two.setName("Iphone x");
        two.setDescription("Just Iphone");
        two.setImage("/phones/2/image");
        two.setPrice(new BigDecimal(1000));
        two.setCount(3);
        items.add(one);
        items.add(two);
        order.setOrder(items);
        return order;
    }

}
