package com.sbuy.order.component;

import com.sbuy.order.exception.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ValidationComponent {

    public boolean orderValidation(List<Long> actual, List<Long> expected) {
        expected.removeAll(actual);
        if (!expected.isEmpty()) {
            throw new EntityNotFoundException(String.format(getErrorText(expected.size()),
                    expected.stream().map(String::valueOf).collect(Collectors.joining(","))));
        }
        return true;
    }

    private String getErrorText(int countOfItem) {
        return countOfItem == 1 ? "Phone with id: %s does not found" : "Phones with id: %s do not found";
    }
}
