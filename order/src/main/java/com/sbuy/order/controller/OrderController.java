package com.sbuy.order.controller;

import com.sbuy.order.dto.request.OrderRequest;
import com.sbuy.order.helper.response.Response;
import com.sbuy.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(value = "/orders")
public class OrderController {

    private OrderService orderService;

    @PostMapping
    public ResponseEntity<?> order(@Valid @RequestBody OrderRequest request) {
        return Response.ok(orderService.order(request));
    }

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }
}
