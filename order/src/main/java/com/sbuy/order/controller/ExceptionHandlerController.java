package com.sbuy.order.controller;

import com.sbuy.order.exception.EntityNotFoundException;
import com.sbuy.order.helper.response.Response;
import com.sbuy.order.helper.response.ValidationError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Slf4j
@ControllerAdvice
@RestController
public class ExceptionHandlerController {

    private MessageSource messageSource;

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<?> handleError(HttpServletRequest req, Exception ex) {
        log.error("Request: {} raised {}. Message: {}.", req.getRequestURL(), ex, ex.getStackTrace());
        return Response.builder().error(ex.getMessage(), HttpStatus.BAD_REQUEST).build();
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<?> handleErrorNotFound(HttpServletRequest req, Exception ex) {
        log.error("Request: {} raised {}. Message: {}.", req.getRequestURL(), ex, ex.getStackTrace());
        return Response.builder().error(ex.getMessage(), HttpStatus.NOT_FOUND).build();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ResponseEntity<?> processValidationError(HttpServletRequest req, MethodArgumentNotValidException ex) {
        return Response.builder().error("Validation error.",
                HttpStatus.UNPROCESSABLE_ENTITY,
                processFieldErrors(ex.getBindingResult().getFieldErrors())).build();
    }

    private List<ValidationError> processFieldErrors(List<FieldError> fieldErrors) {
        List<ValidationError> errors = new ArrayList<>();
        for (FieldError fieldError: fieldErrors) {
            String localizedErrorMessage = resolveLocalizedErrorMessage(fieldError);
            ValidationError error = new ValidationError(fieldError.getField(), localizedErrorMessage, fieldError.getCode(),
                    fieldError.getObjectName());
            errors.add(error);
        }
        return errors;
    }

    private String resolveLocalizedErrorMessage(FieldError fieldError) {
        Locale currentLocale =  LocaleContextHolder.getLocale();
        return messageSource.getMessage(fieldError, currentLocale);
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
}
