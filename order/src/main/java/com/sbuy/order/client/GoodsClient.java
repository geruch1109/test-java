package com.sbuy.order.client;

import com.sbuy.order.dto.base.DataDTO;
import com.sbuy.order.dto.base.PhoneDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "goods-service")
public interface GoodsClient {
    @RequestMapping(method = RequestMethod.GET, value = "/phones", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    DataDTO<List<PhoneDTO>> getPhones(@RequestParam(value = "ids", required = false) List<Long> ids);
}
