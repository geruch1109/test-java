package com.sbuy.order.dto.request;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
public class ItemRequest {
    @NotNull
    private Long itemId;
    @NotNull
    private Integer count;
}
