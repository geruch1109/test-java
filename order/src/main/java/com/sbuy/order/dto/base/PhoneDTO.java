package com.sbuy.order.dto.base;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class PhoneDTO {
    private long id;
    private String name;
    private String description;
    private String image;
    private BigDecimal price;
}
