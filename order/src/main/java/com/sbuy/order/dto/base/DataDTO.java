package com.sbuy.order.dto.base;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DataDTO<T> {
    private T data;
    private int status;
}
