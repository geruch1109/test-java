package com.sbuy.order.service;

import com.sbuy.order.dto.request.OrderRequest;
import com.sbuy.order.model.OrderEntity;

public interface OrderService {
    OrderEntity order(OrderRequest order);
}
