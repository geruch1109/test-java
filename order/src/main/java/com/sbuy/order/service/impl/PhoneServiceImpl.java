package com.sbuy.order.service.impl;

import com.sbuy.order.client.GoodsClient;
import com.sbuy.order.component.ValidationComponent;
import com.sbuy.order.dto.base.PhoneDTO;
import com.sbuy.order.service.PhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PhoneServiceImpl implements PhoneService {

    private GoodsClient goodsClient;

    private ValidationComponent validation;

    @Override
    public List<PhoneDTO> find(List<Long> ids) {
        return goodsClient.getPhones(ids).getData();
    }

    @Override
    public List<PhoneDTO> findAndValidate(List<Long> ids) {
        List<PhoneDTO> phones =  find(ids);
        validation.orderValidation(phones.stream().map(PhoneDTO::getId).collect(Collectors.toList()), ids);
        return phones;
    }


    @Autowired
    public void setGoodsClient(GoodsClient goodsClient) {
        this.goodsClient = goodsClient;
    }

    @Autowired
    public void setValidation(ValidationComponent validation) {
        this.validation = validation;
    }
}
