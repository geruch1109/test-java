package com.sbuy.order.service;

import com.sbuy.order.dto.base.PhoneDTO;

import java.util.List;

public interface PhoneService {
    List<PhoneDTO> find(List<Long> ids);
    List<PhoneDTO> findAndValidate(List<Long> ids);
}
