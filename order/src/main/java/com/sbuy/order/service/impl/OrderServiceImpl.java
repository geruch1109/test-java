package com.sbuy.order.service.impl;

import com.sbuy.order.dto.request.ItemRequest;
import com.sbuy.order.dto.request.OrderRequest;
import com.sbuy.order.dto.base.PhoneDTO;
import com.sbuy.order.model.ItemEntity;
import com.sbuy.order.model.OrderEntity;
import com.sbuy.order.service.OrderService;
import com.sbuy.order.service.PhoneService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    private PhoneService phoneService;

    @Override
    public OrderEntity order(OrderRequest order) {
        List<Long> orderIds = order.getOrder().stream().map(ItemRequest::getItemId).collect(Collectors.toList());
        return generateOrder(order, phoneService.findAndValidate(orderIds));
    }

    private OrderEntity generateOrder(OrderRequest order, List<PhoneDTO> phones) {
        Map<Long, PhoneDTO> phoneMap = phones.stream().collect(Collectors.toMap(PhoneDTO::getId, e -> e));
        OrderEntity entity = new OrderEntity();
        BeanUtils.copyProperties(order, entity, "order");
        entity.setOrder(order.getOrder().stream().map(item -> fillItem(item, phoneMap.get(item.getItemId())))
                .collect(Collectors.toList()));
        return entity;
    }

    private ItemEntity fillItem(ItemRequest item, PhoneDTO phone) {
        ItemEntity i = new ItemEntity();
        i.setCount(item.getCount());
        Optional.ofNullable(phone).ifPresent(p -> BeanUtils.copyProperties(p, i));
        return i;
    }

    @Autowired
    public void setPhoneService(PhoneService phoneService) {
        this.phoneService = phoneService;
    }
}
