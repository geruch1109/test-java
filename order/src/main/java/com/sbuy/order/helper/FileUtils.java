package com.sbuy.order.helper;

public class FileUtils {
    private static final String FILE_BASE_PATH = "resources/phones/";
    private static final String FILE_BASE_URL = "/phones/{id}/image";
    public static final String DEFAUL_MIME_TYPE = "image/jpeg";

    public static String getFilePath(String name) {
        return FILE_BASE_PATH.concat(name);
    }

    public static String getFileUrl(long id) {
        return FILE_BASE_URL.replace("{id}", String.valueOf(id));
    }
}
