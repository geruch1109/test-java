package com.sbuy.order.helper.response;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Response {

    public static ResponseBuilder builder() {
        return new ResponseBuilder();
    }

    public static ResponseEntity ok(Object object) {
        ResponseBuilder builder = new ResponseBuilder().data(object);
        return builder.build();
    }

    public static ResponseEntity ok(File file, String fileName, String mimeType) throws FileNotFoundException {
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("content-disposition", "inline; filename=" + fileName);
        responseHeaders.add("Content-Type", mimeType);
        return ResponseEntity.ok()
                .headers(responseHeaders)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType(mimeType))
                .body(resource);
    }

    public static ResponseEntity status(HttpStatus status) {
        return new ResponseBuilder().status(status).build();
    }


    public static class ResponseBuilder {
        private final String DATA = "data";
        private final String STATUS_CODE = "status";
        private final String MESSAGE = "message";
        private final String ERRORS = "errors";
        private final String META = "meta";

        private HttpStatus status = HttpStatus.OK;
        private Map<String, Object> body;

        public ResponseBuilder() {
            createBodyIfEmpty();
        }

        public ResponseBuilder data(Object data) {
            body.put(DATA, data);
            return this;
        }

        public ResponseBuilder pagination(int page, int limit, long total) {
            body.put(META, new MetaData(page, total, limit));
            return this;
        }

        public ResponseBuilder status(HttpStatus status) {
            this.status = status;
            body.put(STATUS_CODE, status.value());
            return this;
        }

        public ResponseBuilder error(String message, HttpStatus status) {
            body.put(STATUS_CODE, status.value());
            body.put(MESSAGE, message);
            this.status = status;
            return this;
        }

        public ResponseBuilder error(String message, HttpStatus status, List<ValidationError> errors) {
            body.put(STATUS_CODE, status.value());
            body.put(MESSAGE, message);
            body.put(ERRORS, errors);
            this.status = status;
            return this;
        }

        public ResponseBuilder message(String title, HttpStatus status) {
            body.put(STATUS_CODE, status.value());
            body.put(MESSAGE, title);
            this.status = status;
            return this;
        }

        public ResponseBuilder put(String key, Object value) {
            createBodyIfEmpty();
            body.put(key, value);
            return this;
        }

        private void createBodyIfEmpty() {
            if (Objects.isNull(body)) {
                body = new HashMap<>();
                body.put(STATUS_CODE, status.value());
            }
        }

        public ResponseEntity build() {
            return ResponseEntity.status(status).body(body);
        }

        public ResponseEntity build(Object data) {
            this.data(data);
            return ResponseEntity.status(status).body(body);
        }

    }

}
