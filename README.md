# Simple buy

**A simple way to buy phones**

## Functional services

sBuy was decomposed into two core microservices. All of them are independently deployable applications, organized around certain business domains.

#### Goods service
Contains logic related to retrieve phones and images.

Method	| Path	| Description	| 
------------- | ------------------------- | ------------- |
GET	| /phones	| Get specified phones data	|
GET	| /phones/{id}/image	| Get image related to phone	|

#### Order service
Contains logic related to order phones and validation.

Method	| Path	| Description	| 
------------- | ------------------------- | ------------- |
POST	| /orders	| Make order	|

## Infrastructure services
There's a bunch of common patterns in distributed systems, which could help us to make described core services work. Spring cloud provides powerful tools that enhance Spring Boot applications behaviour to implement those patterns. I'll cover them briefly.

### API Gateway
As you can see, there are three core services, which expose external API to client. In a real-world systems, this number can grow very quickly as well as whole system complexity. Actually, hundreds of services might be involved in rendering of one complex webpage.

In theory, a client could make requests to each of the microservices directly. But obviously, there are challenges and limitations with this option, like necessity to know all endpoints addresses, perform http request for each peace of information separately, merge the result on a client side. Another problem is non web-friendly protocols which might be used on the backend.

Usually a much better approach is to use API Gateway. It is a single entry point into the system, used to handle requests by routing them to the appropriate backend service or by invoking multiple backend services and aggregating the results. Also, it can be used for authentication, insights, stress and canary testing, service migration, static response handling, active traffic management.

Netflix opensourced such an edge service, and now with Spring Cloud we can enable it with one `@EnableZuulProxy` annotation. In this project, I use Zuul to store static content (ui application) and to route requests to appropriate microservices. Here's a simple prefix-based routing configuration for Notification service:

```yml
zuul:
  ignoredServices: '*'
  host:
    connect-timeout-millis: 20000
    socket-timeout-millis: 20000

  routes:
    goods-service:
      path: /phones/**
      serviceId: goods-service
      stripPrefix: false
      sensitiveHeaders:

    order-service:
      path: /orders/**
      serviceId: order-service
      stripPrefix: false
      sensitiveHeaders:

```

That means all requests starting with `/phones` will be routed to Goods-service service. There is no hardcoded address, as you can see. Zuul uses Service discovery mechanism to locate Notification service instances and also Load Balancer, described below.

### Service discovery

Another commonly known architecture pattern is Service discovery. It allows automatic detection of network locations for service instances, which could have dynamically assigned addresses because of auto-scaling, failures and upgrades.

The key part of Service discovery is Registry. I use Netflix Eureka in this project. Eureka is a good example of the client-side discovery pattern, when client is responsible for determining locations of available service instances (using Registry server) and load balancing requests across them.

With Spring Boot, you can easily build Eureka Registry with `spring-cloud-starter-eureka-server` dependency, `@EnableEurekaServer` annotation and simple configuration properties.

Client support enabled with `@EnableDiscoveryClient` annotation an `bootstrap.yml` with application name:
``` yml
spring:
  application:
    name: goods-service
```

Now, on application startup, it will register with Eureka Server and provide meta-data, such as host and port, health indicator URL, home page etc. Eureka receives heartbeat messages from each instance belonging to a service. If the heartbeat fails over a configurable timetable, the instance will be removed from the registry.

Also, Eureka provides a simple interface, where you can track running services and a number of available instances: `http://localhost:8761`

### Ribbon
Ribbon is a client side load balancer which gives you a lot of control over the behaviour of HTTP and TCP clients. Compared to a traditional load balancer, there is no need in additional hop for every over-the-wire invocation - you can contact desired service directly.
Out of the box, it natively integrates with Spring Cloud and Service Discovery. Eureka Client provides a dynamic list of available servers so Ribbon could balance between them.

### Feign
Feign is a declarative Http client, which seamlessly integrates with Ribbon and Hystrix. Actually, with one `spring-cloud-starter-feign` dependency and `@EnableFeignClients` annotation you have a full set of Load balancer, Http client with sensible ready-to-go default configuration.

Here is an example from Order Service:

``` java
@FeignClient(name = "goods-service")
public interface GoodsClient {
    @RequestMapping(method = RequestMethod.GET, value = "/phones", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    DataDTO<List<PhoneDTO>> getPhones(@RequestParam(value = "ids", required = false) List<Long> ids);
}
```

- Everything you need is just an interface
- You can share `@RequestMapping` part between Spring MVC controller and Feign methods
- Above example specifies just desired service id - `goods-service`, thanks to autodiscovery through Eureka (but obviously you can access any resource with a specific url)

## How to run all the things?

#### Dev mode

Run microservices with spring.profile.active=dev environment variable.

#### Docker mode
1. Build the project
    mvn install -DskipTests
2. Build docker containers
    docker build --tag registry-service --rm=true .
    docker build --tag gateway-service --rm=true .
    docker build --tag goods-service  --rm=true .
    docker build --tag order-service --rm=true .
3. Create network
    docker network create -d bridge sbuy
4. Run mysql container
    docker run  -e MYSQL_ROOT_PASSWORD=root  -e MYSQL_DATABASE=goods  -e MYSQL_USER=root   -e MYSQL_PASSWORD=root   --mount type=volume,src=crv_mysql,dst=/var/lib/mysql  -d --name sql-server  -p 3306:3306 --network=sbuy  mysql:latest
5. Run other containers
    docker run -d --name registry --network=sbuy -p 8761:8761  registry-service
    docker run -d --name gateway --network=sbuy -p 4000:4000 gateway-service
    docker run -d --name goods --network=sbuy -p 5050:5050 goods-service
    docker run -d --name order --network=sbuy -p 5055:5055 order-service
    
#### Important endpoints
- http://localhost:4000 - Gateway
- http://localhost:8761 - Eureka Dashboard

##Question

###1.How would you improve the system?
The first thing is we can start to use CDN - that service decrease load time of images and static content.
Second - start to use load balancer it will increase bandwidth.
Then we have to close the outside access to the microservice except for gateway service.
Also, we can start to use some SSO and auth for secure endpoints and last important thing is use health checker(Hystrix) - it provide information about performance and other stuff 

###2. How would you avoid your order API to be overflow?
As I said before start to use load balancer 