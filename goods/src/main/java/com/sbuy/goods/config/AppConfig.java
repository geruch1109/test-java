package com.sbuy.goods.config;


import com.sbuy.goods.dto.Phone;
import com.sbuy.goods.model.PhoneEntity;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public ModelMapper getModelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.typeMap(PhoneEntity.class, Phone.class)
                .addMapping(PhoneEntity::getPath, Phone::setImage);

        return modelMapper;
    }

}
