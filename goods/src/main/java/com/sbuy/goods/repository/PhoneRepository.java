package com.sbuy.goods.repository;

import com.sbuy.goods.model.PhoneEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PhoneRepository extends JpaRepository<PhoneEntity, Long> {

    Page<PhoneEntity> findAll(Pageable pageable);

    List<PhoneEntity> findAllByIdIn(List<Long> ids);
}
