package com.sbuy.goods.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class Phone {
    private long id;
    private String name;
    private String description;
    private String image;
    private BigDecimal price;
}
