package com.sbuy.goods.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {

    private String id;
    private Class<?> entityClass;

    public EntityNotFoundException(String id, Class<?> entityClass) {
        this.id = id;
        this.entityClass = entityClass;
    }

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityNotFoundException(Throwable cause) {
        super(cause);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Class<?> getEntityClass() {
        return entityClass;
    }

    public void setEntityClass(Class<?> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public String getMessage() {
        String message = super.getMessage();

        if (message != null) {
            return message;
        }

        String entityName = entityClass == null ? "Entity" : entityClass.getSimpleName().replace("Entity", "");
        return String.format("%s with id %s not found", entityName, id);
    }
}
