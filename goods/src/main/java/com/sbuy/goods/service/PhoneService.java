package com.sbuy.goods.service;

import com.sbuy.goods.model.PhoneEntity;
import org.springframework.data.domain.Page;

import java.util.List;

public interface PhoneService {

    Page<PhoneEntity> find(int page, int count);
    List<PhoneEntity> find(List<Long> ids);
    PhoneEntity find(Long id);

}
