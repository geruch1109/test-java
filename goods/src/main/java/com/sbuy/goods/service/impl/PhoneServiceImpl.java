package com.sbuy.goods.service.impl;

import com.sbuy.goods.exception.EntityNotFoundException;
import com.sbuy.goods.model.PhoneEntity;
import com.sbuy.goods.repository.PhoneRepository;
import com.sbuy.goods.service.PhoneService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

@Slf4j
@Service
public class PhoneServiceImpl implements PhoneService {

    private PhoneRepository phoneRepository;

    @Override
    public Page<PhoneEntity> find(int page, int count) {
        Pageable pageable = PageRequest.of(page - 1, count, Sort.by(Sort.Direction.ASC, "id"));
        return phoneRepository.findAll(pageable);
    }

    @Override
    public List<PhoneEntity> find(List<Long> ids) {
        return phoneRepository.findAllByIdIn(ids);
    }

    @Override
    public PhoneEntity find(Long id) {
        return phoneRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Phone is not found"));
    }

    @Autowired
    public void setPhoneRepository(PhoneRepository phoneRepository) {
        this.phoneRepository = phoneRepository;
    }
}
