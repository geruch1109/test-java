package com.sbuy.goods.controller;

import com.sbuy.goods.dto.Phone;
import com.sbuy.goods.exception.EntityNotFoundException;
import com.sbuy.goods.helper.FileUtils;
import com.sbuy.goods.helper.response.Response;
import com.sbuy.goods.model.PhoneEntity;
import com.sbuy.goods.service.PhoneService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping(value = "/phones")
public class PhoneController {

    private PhoneService phoneService;

    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<?> find(@RequestParam(value = "count", required = false, defaultValue = "10") int count,
                                  @RequestParam(value = "page", required = false, defaultValue = "1") int page,
                                  @RequestParam(value = "ids", required = false) List<Long> ids) {
        log.debug("Retrieve set of phones");
        Response.ResponseBuilder response = Response.builder();
        return Optional.ofNullable(ids).map(e -> response.data(phoneService.find(e)
                .stream()
                .map(phone -> modelMapper.map(phone, Phone.class))
                .collect(Collectors.toList())).build())
            .orElseGet(() -> {
                Page<PhoneEntity> pages = phoneService.find(page, count);
                return response.pagination(page, pages.getNumberOfElements(), pages.getTotalPages())
                    .data(pages.getContent()
                        .stream()
                        .map(phone -> modelMapper.map(phone, Phone.class))
                        .collect(Collectors.toList())).build();
            });
    }

    @RequestMapping(value = "/{id}/image", method = RequestMethod.GET)
    public ResponseEntity<?> getPhoneImage(@PathVariable("id") long id) {
        log.debug("Getting file: {}", id);
        PhoneEntity phone = phoneService.find(id);
        try {
            File file = new File(FileUtils.getFilePath(phone.getImage()));
            return Response.ok(file, phone.getImage(), FileUtils.DEFAUL_MIME_TYPE);
        } catch (IOException e) {
            throw new EntityNotFoundException("File did not find");
        }
    }

    @Autowired
    public void setPhoneService(PhoneService phoneService) {
        this.phoneService = phoneService;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }
}
