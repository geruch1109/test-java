package com.sbuy.goods.controller;

import com.sbuy.goods.exception.EntityNotFoundException;
import com.sbuy.goods.helper.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@ControllerAdvice
@RestController
public class ExceptionHandlerController {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<?> handleError(HttpServletRequest req, Exception ex) {
        log.error("Request: {} raised {}. Message: {}.", req.getRequestURL(), ex, ex.getStackTrace());
        return Response.builder().error(ex.getMessage(), HttpStatus.BAD_REQUEST).build();
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<?> handleErrorNotFound(HttpServletRequest req, Exception ex) {
        log.error("Request: {} raised {}. Message: {}.", req.getRequestURL(), ex, ex.getStackTrace());
        return Response.builder().error(ex.getMessage(), HttpStatus.NOT_FOUND).build();
    }
}
