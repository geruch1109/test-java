package com.sbuy.goods.unit;

import com.sbuy.goods.model.PhoneEntity;
import com.sbuy.goods.service.PhoneService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PhoneServiceTest {

    @Autowired
    private PhoneService phoneService;

    @Test
    public void retrieveFirstFiveElementTrue() {
        assertEquals(5, phoneService.find(1, 5).getContent().size());
    }

    @Test
    public void retrieveFirstFiveElementFalse() {
        assertNotEquals(5, phoneService.find(2, 5).getContent().size());
    }

    @Test
    public void retrieveTwoElementsByIdsTrue() {
        List<Long> ids = phoneService.find(1, 2).getContent().stream().map(PhoneEntity::getId).collect(Collectors.toList());
        Map<Long, PhoneEntity> phones = phoneService.find(ids).stream().collect(Collectors.toMap(PhoneEntity::getId,  e -> e));
        ids.forEach(id -> assertTrue(Objects.nonNull(phones.get(id))));
    }

    @Test
    public void returnEmptyListA() {
        assertTrue(phoneService.find(5, 5).getContent().isEmpty());
    }

    @Test
    public void returnEmptyListB() {
        assertTrue(phoneService.find(Arrays.asList(999999L, 9999988L)).isEmpty());
    }
}
